/**
 * @file
 * Defines Javascript behaviors for the cookies module.
 */
(function (Drupal, $) {
  'use strict';

  /**
   * Define defaults.
   */
  Drupal.behaviors.cookiesFacebook = {
    initFacebook : {},

    activate: function (context) {
      $('script[data-sid="facebook"]').each(function () {
        var replacement = $(this).clone().removeAttr('type').removeAttr('data-sid');
        $(this).replaceWith(replacement.prop('outerHTML'));
      });
    },

    fallback: function (context) {
      $('.facebook-embedded-content').cookiesOverlay('facebook');
    },

    attach: function (context) {
      var self = this;
      if (Drupal.behaviors.hasOwnProperty('facebookMediaEntity')) {
        // Take over the init function and remove it from the original context.
        if (typeof Drupal.behaviors.facebookMediaEntity.attach === 'function') {
          self.initFacebook['attach'] = Drupal.behaviors.facebookMediaEntity.attach;
          self.initFacebook.attach.bind(self.initFacebook);
          Drupal.behaviors.facebookMediaEntity.attach = null;
        }
      }

      document.addEventListener('cookiesjsrUserConsent', function (event) {
        var service = (typeof event.detail.services === 'object') ? event.detail.services : {};
        if (typeof service.facebook !== 'undefined' && service.facebook) {
          self.activate(context);
        } else {
          self.fallback(context);
        }
      });
    }
  };
})(Drupal, jQuery);
